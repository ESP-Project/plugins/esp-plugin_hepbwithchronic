'''
                                  ESP Health
                        Hepatitis B with Chronic Hep B
                             Packaging Information


@author: Jeff Andre <jandre@commoninf.com>
@organization: Commonwealth Informatics https://www.commoninf.com
@contact: http://www.esphealth.org
@copyright: (c) 2021 Commonwealth Informatics, Inc.
@license: LGPL
'''

from setuptools import find_packages
from setuptools import setup

setup(
    name='esp-plugin_hepbwithchronic',
    version='1.2',
    author='Jeff Andre',
    author_email='jandre@commoninf.com',
    description='Hepatitis B with Chronic Hep B disease definition module for ESP Health application',
    license='LGPLv3',
    keywords='hepatitis b algorithm disease surveillance public health epidemiology',
    url='http://esphealth.org',
    packages=find_packages(exclude=['ez_setup']),
    install_requires=[
    ],
    entry_points='''
        [esphealth]
        disease_definitions = hepbwithchronic:disease_definitions
        event_heuristics = hepbwithchronic:event_heuristics
    '''
)
