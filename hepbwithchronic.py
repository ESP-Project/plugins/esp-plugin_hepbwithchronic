'''
                                  ESP Health
                        Hepatitis B with Chronic Hep B
                              Disease Definition


@author: Jeff Andre <jandre@commoninf.com>
@organization: Commonwealth Informatics https://www.commoninf.com
@contact: http://www.esphealth.org
@copyright: (c) 2021 Commonwealth Informatics, Inc.
@license: LGPL
'''

from collections import defaultdict
from decimal import Decimal
import datetime

from dateutil.relativedelta import relativedelta
from django.db.models import Max

from ESP.hef.base import AbstractLabTest
from ESP.hef.base import BaseEventHeuristic
from ESP.hef.base import DiagnosisHeuristic
from ESP.hef.base import Dx_CodeQuery
from ESP.hef.base import LabResultFixedThresholdHeuristic
from ESP.hef.base import LabResultPositiveHeuristic, LabResultNoEventHeuristic
from ESP.hef.base import LabResultRatioHeuristic
from ESP.hef.models import Timespan
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import CaseActiveHistory, Case
from ESP.utils import log


class Hepatitis_B(DiseaseDefinition):
    '''
    Hepatitis B with Chronic Hep B
    '''

    # disease definitions detect acute hep-b, and chronic hep-b
    conditions = ['hepatitis_b']

    uri = 'urn:x-esphealth:disease:channing:hepatitis-combined:hepbwithchronic:v1'

    short_name = 'hepatitis_b'

    test_name_search_strings = [
        'hep',
        'alt',
        'ast',
        'bili',
        'tbil',
        'hbc',
        'hbv',
        'hbsag',
        'sgpt',
        'sgot',
        'aminotrans'
    ]

    timespan_heuristics = []


    @property
    def event_heuristics(self):
        '''
        Event heuristics used by all Hepatitis variants
        '''
        heuristic_list = []
        heuristic_list.append(LabResultFixedThresholdHeuristic(
            test_name='bilirubin_total',
            threshold=Decimal('1.5'),
            match_type='gt',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_b_surface_antigen',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_b_e_antigen',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_b_viral_dna',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='bilirubin_direct',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='bilirubin_indirect',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_b_core_antigen_igm_antibody',
        ))
        heuristic_list.append(LabResultRatioHeuristic(
            test_name='alt',
            ratio=2.5,
        ))
        heuristic_list.append(LabResultRatioHeuristic(
            test_name='ast',
            ratio=2.5,
        ))

        # Reportable Only - No event
        heuristic_list.append(LabResultNoEventHeuristic(
            test_name= 'hepatitis_b_core_ab',
        ))


        heuristic_list.append(DiagnosisHeuristic(
            name='jaundice',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='R17', type='icd10'),
                Dx_CodeQuery(starts_with='782.4', type='icd9'),
            ]
        ))
        heuristic_list.append(DiagnosisHeuristic(
            name='hepatitis_b:chronic',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='B18.1', type='icd10'),
                Dx_CodeQuery(starts_with='B18.0', type='icd10'),
                Dx_CodeQuery(starts_with='070.32', type='icd9'),
            ]
        ))

        return heuristic_list

    def generate(self):
        log.info('Generating cases for %s (%s)' % (self.short_name, self.uri))
        counter = 0
        counter += self.generate_definition_a()
        counter += self.generate_definition_b_c()
        counter += self.generate_definition_d()

        chronic_counter = self.generate_definition_chronic()
        counter += chronic_counter

        return counter

    def generate_definition_a(self):
        '''
        a) (#1 or #2 or #3) AND #4 within 14 day period
        1. ICD9 = 782.4 (jaundice, not of newborn)
        2. Alanine aminotransferase (ALT) >2.5x upper limit of normal
        3. Aspartate aminotransferase (AST) >2.5x upper limit of normal
        4. IgM antibody to Hepatitis B Core Antigen = "REACTIVE" (may be truncated)
        '''
        log.info('Generating cases for Hepatitis B definition A')
        trigger_qs = BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_core_antigen_igm_antibody:positive')
        trigger_qs = trigger_qs.exclude(case__condition=self.conditions[0])
        trigger_qs = trigger_qs.order_by('date')
        confirmation_qs = BaseEventHeuristic.get_events_by_name(name='dx:jaundice')
        confirmation_qs |= BaseEventHeuristic.get_events_by_name(name='lx:alt:ratio:2.5')
        confirmation_qs |= BaseEventHeuristic.get_events_by_name(name='lx:ast:ratio:2.5')
        confirmation_qs = confirmation_qs.order_by('date')
        counter = 0
        for trigger_event in trigger_qs:
            pat = trigger_event.patient
            begin_relevancy = trigger_event.date - relativedelta(days=14)
            end_relevancy = trigger_event.date + relativedelta(days=14)
            pat_conf_qs = confirmation_qs.filter(
                patient=pat,
                date__gte=begin_relevancy,
                date__lte=end_relevancy,
            )
            if not pat_conf_qs:
                # This patient does not have Hep B
                continue
            created, this_case = self._create_case_from_event_obj(
                condition=self.conditions[0],
                criteria='Criteria A: [positive Hep B core antigen igm antibody and (jaundice (not of newborn) or ast>2.5x ULN or alt >2.5x ULN)] w/in 14 days',
                recurrence_interval=None,  # Does not recur
                event_obj=trigger_event,
                relevant_event_qs=pat_conf_qs,
            )
            if created:
                counter += 1
                CaseActiveHistory.create(
                        case=this_case,
                        status="HEP_B-A",
                        date=this_case.date,
                        change_reason="HEP_B-AI",
                        event_rec=trigger_event,
                )
        log.debug('Created %s new Hep B definition a cases' % counter)
        return counter

    def generate_definition_b_c(self):
        '''
        b) (#1 or #2 or #3) AND (#12 or #15) AND #5 "reactive" within 21 day period 
            AND no prior positive result for #5 or #7 ever 
            AND no code for chronic hep b ICD9=070.32 at this encounter or in patient's past
        c) (#1 or #2 or #3) AND (#12 or #15) AND #7  positive within 21 day period 
            AND no prior positive result for #5 or #7 ever 
            AND no code for chronic hep b ICD9=070.32 at this encounter or in the patient's past
        1. ICD9 = 782.4 (jaundice, not of newborn)
        2. Alanine aminotransferase (ALT) >2.5x upper limit of normal
        3. Aspartate aminotransferase (AST) >2.5x upper limit of normal
        5. Hepatitis B Surface Antigen
        7. Hepatitis B Viral DNA
        12. Total bilirubin > 1.5
        15. Calculated bilirubin = (direct bilirubin + indirect bilirubin) = value > 1.5
        ICD9 070.32 = Chronic Hep B
        '''
        log.info('Generating cases for Hepatitis B definitions B/C')
        counter = 0
        #
        # Surface antigen test and viral DNA test are the trigger events (#5 or #7)
        #
        hep_b_pos_qs = BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_surface_antigen:positive')
        hep_b_pos_qs |= BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_viral_dna:positive')
        #
        # Unbound positives are trigger events
        #
        trigger_qs = hep_b_pos_qs.exclude(case__condition=self.conditions[0])
        trigger_qs = trigger_qs.order_by('date')
        #
        # Chronic Hep B diagnosis
        #
        chronic_hep_b_qs = BaseEventHeuristic.get_events_by_name(name='dx:hepatitis_b:chronic')
        #
        # Jaundice and associated high ALT/AST results (#1 or #2 or #3)
        #
        jaundice_qs = BaseEventHeuristic.get_events_by_name(name='dx:jaundice')
        jaundice_qs |= BaseEventHeuristic.get_events_by_name(name='lx:alt:ratio:2.5')
        jaundice_qs |= BaseEventHeuristic.get_events_by_name(name='lx:ast:ratio:2.5')
        #
        # Bilirubin - total bilirubin events can be queries directly; but 
        # calculated bilirubin must be, ahem, calculated programmatically.
        #
        total_bili_qs = BaseEventHeuristic.get_events_by_name(name='lx:bilirubin_total:threshold:gt:1.5')
        direct_bili_labs_qs = AbstractLabTest('bilirubin_direct').lab_results
        indirect_bili_labs_qs = AbstractLabTest('bilirubin_indirect').lab_results

        for trigger_event in trigger_qs:
            patient = trigger_event.patient
            date = trigger_event.date
            relevancy_start = date - relativedelta(days=21)
            relevancy_end = date + relativedelta(days=21)
            #
            # No chronic hep B diagnosis
            #
            if chronic_hep_b_qs.filter(patient=patient, date__lte=date):
                continue  # Patient has Chronic Hep B

            #
            # No prior Hep B test positive
            #
            if hep_b_pos_qs.filter(patient=patient, date__lt=date):
                continue  # Patient has Chronic Hep B
            #
            # Patient must have jaundice or high ALT/AST results
            #
            pat_jaundice_qs = jaundice_qs.filter(
                patient=patient,
                date__gte=relevancy_start,
                date__lte=relevancy_end,
            )
            if not pat_jaundice_qs:
                continue  # Patient does not have Hep B
            #
            # Patient must have elevated bilirubin values.  We check the total
            # bilirubin event first since it's a simple query.  If it is not 
            # found, we check calculated bilirubin.
            #
            pat_total_bili_qs = total_bili_qs.filter(
                patient=patient,
                date__gte=relevancy_start,
                date__lte=relevancy_end,
            )
            pat_direct_bili_qs = direct_bili_labs_qs.filter(
                patient=patient,
                date__gte=relevancy_start,
                date__lte=relevancy_end,
            )
            pat_indirect_bili_qs = indirect_bili_labs_qs.filter(
                patient=patient,
                date__gte=relevancy_start,
                date__lte=relevancy_end,
            )
            either_bili_qs = direct_bili_labs_qs | indirect_bili_labs_qs
            either_bili_qs = either_bili_qs.filter(
                patient=patient,
                date__gte=relevancy_start,
                date__lte=relevancy_end,
            ).order_by('date')
            if pat_total_bili_qs:
                criteria = 'total bilirubin>1.5'
            else:
                criteria = 'calculated bilirubin >1.5'
                calculated_bilirubin = 0
                bili_date_list = either_bili_qs.values_list('date', flat=True)
                for bili_date in bili_date_list:
                    max_direct_dict = pat_direct_bili_qs.filter(date=bili_date).aggregate(Max('result_float'))
                    if max_direct_dict['result_float__max']:
                        max_direct = max_direct_dict['result_float__max']
                    else:
                        max_direct = 0
                    max_indirect_dict = pat_indirect_bili_qs.filter(date=bili_date).aggregate(Max('result_float'))
                    if max_indirect_dict['result_float__max']:
                        max_indirect = max_indirect_dict['result_float__max']
                    else:
                        max_indirect = 0
                    total_this_date = max_direct + max_indirect
                    if total_this_date > calculated_bilirubin:
                        calculated_bilirubin = total_this_date
                if not calculated_bilirubin > 1.5:
                    continue  # Patient does not have Hep B

            created, this_case = self._create_case_from_event_obj(
                condition=self.conditions[0],
                criteria='Criteria B/C: [(jaundice (not of newborn) or alt>2.5x ULN or ast >2.5x ULN) and (positive of hep_b_surface or positive of hep_b_viral_dna) and (%s) ]  w/in 21 days;  exclude if: prior/current dx=chronic hepatitis B or prior positive hep_b_surface ever or prior positive hep_b_viral_dna ever' % criteria,
                recurrence_interval=None,  # Does not recur
                event_obj=trigger_event,
                relevant_event_qs=jaundice_qs | total_bili_qs
            )
            if created:
                counter += 1
                CaseActiveHistory.create(
                        case=this_case,
                        status="HEP_B-A",
                        date=this_case.date,
                        change_reason="HEP_B-AI",
                        event_rec=trigger_event,
                )
        log.debug('Created %s new Hep B cases for defintions B/C' % counter)
        return counter

    def generate_definition_d(self):
        '''
        d) #5 "reactive" with record of #5 "non-reactive" within the prior 6 months 
	        AND no prior positive test for #5 or #7 ever 
	        AND no code for ICD9=070.32 at this encounter or in patient's past.  
        Please use "date collected" (or if unavailable then "date ordered") for 
        comparison of dates.
        5. Hepatitis B Surface Antigen = "REACTIVE" (may be truncated)
        7. Hepatitis B Viral DNA
        '''
        log.info('Generating cases for Hep B definition D')
        counter = 0
        surface_pos_qs = BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_surface_antigen:positive')
        surface_neg_qs = BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_surface_antigen:negative')
        viral_pos_qs = BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_viral_dna:positive')
        chronic_dx_qs = BaseEventHeuristic.get_events_by_name(name='dx:hepatitis_b:chronic')
        unbound_surface_pos_qs = surface_pos_qs.exclude(case__condition__in=self.conditions)
        #
        # Patient must have a positive Hep B surface antigen test
        #
        for surface_pos_event in unbound_surface_pos_qs.order_by('date'):
            #
            # Patient must have a negative surface antigen result in the past 6 months
            #
            relevancy_begin = surface_pos_event.date - relativedelta(months=6)
            prior_neg_qs = surface_neg_qs.filter(patient=surface_pos_event.patient)
            prior_neg_qs = prior_neg_qs.filter(date__lt=surface_pos_event.date)
            prior_neg_qs = prior_neg_qs.filter(date__gte=relevancy_begin)
            if not prior_neg_qs:
                # Patient could possibly have Hep B, but existing EMR data is 
                # not sufficient to confirm a case.
                continue
                #
            # Exclude patient if they have a prior surface antigen or viral dna test
            #
            prior_pos_qs = surface_pos_qs | viral_pos_qs
            prior_pos_qs = prior_pos_qs.filter(patient=surface_pos_event.patient)
            prior_pos_qs = prior_pos_qs.filter(date__lt=surface_pos_event.date)
            if prior_pos_qs:
                continue  # Patient does not have acute Hep B - probably has chronic
            #
            # Exclude patient if they have a chronic hep b diagnosis at this time or earlier.
            #
            if chronic_dx_qs.filter(patient=surface_pos_event.patient, date__lte=surface_pos_event.date):
                continue  # Patient has chronic hep b
            #
            # Patient has acute hep b!
            #
            created, this_case = self._create_case_from_event_obj(
                condition=self.conditions[0],
                criteria='Criteria D: positive hep b surface and prior neg of hep b surface w/in 6 months; exclude if:  prior/current dx=chronic hepatitis B or prior positive hep_b_surface ever or prior positive hep_b_viral_dna ever',
                recurrence_interval=None,  # Does not recur
                event_obj=surface_pos_event,
                relevant_event_qs=prior_neg_qs,
            )
            if created:
                counter += 1
                CaseActiveHistory.create(
                        case=this_case,
                        status="HEP_B-A",
                        date=this_case.date,
                        change_reason="HEP_B-AI",
                        event_rec=surface_pos_event,
                )
        log.debug('Created %s new Hep B definition D' % counter)
        return counter

    def generate_definition_chronic(self):
        '''
        Classify patient as having chronic hepatitis B if any of the following conditions are true:
            e) Any patient with (#5 or #6 or #7) who does not fulfill criteria for acute hepatitis B (above) 
            f) Any patient with (#5 or #6 or #7) who fulfilled criteria for acute hepatitis B more than 
            6months prior to the current positive test (#5 or #6 or #7)
            g) patients with diagnosis of chronic Hep B within 30 days of acute hep-b (criteria B/C)
        '''
        log.info('Generating cases for Chronic Hep B definitions e,f,g')
        counter = 0
        hep_b_pos_qs = BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_surface_antigen:positive')
        hep_b_pos_qs |= BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_e_antigen:positive')
        hep_b_pos_qs |= BaseEventHeuristic.get_events_by_name(name='lx:hepatitis_b_viral_dna:positive')
        hep_b_pos_qs |= BaseEventHeuristic.get_events_by_name(name='dx:hepatitis_b:chronic')
        hep_b_pos_qs = hep_b_pos_qs.order_by('date')

        #
        # Unbound positives are trigger events
        #
        hep_b_events = hep_b_pos_qs.exclude(case__condition=self.conditions[0])

        # chronic hep-b
        for event in hep_b_events:
            #
            # Check for acute hep-b cases that become chronic hep-b
            #
            hep_b_case = Case.objects.filter(patient_id=event.patient.id, condition=self.conditions[0]).last()
            updated = False
            if hep_b_case:
                # Get caseactivehistory (last one has current status)
                case_ah_obj = CaseActiveHistory.objects.filter(case_id= hep_b_case.id).last()
                if event.name == 'dx:hepatitis_b:chronic':
                    #
                    # Cases with chronic hep-b diagnosis within 30 days of acute hep-b (criteria B/C)
                    #
                    if (case_ah_obj and (case_ah_obj.status == 'HEP_B-A') and
                        ("Criteria B/C" in hep_b_case.criteria) and 
                        (event.date <= hep_b_case.date + relativedelta(days=30)) and
                        (event.date >= hep_b_case.date) ):
                            new_criteria = 'Criteria G: chronic hep-b diagnosis within 30 days of acute hep-b (criteria b/c)'
                            updated = self.update_case(hep_b_case,new_criteria, event)
                else:
                    #
                    # Cases with acute hep-b more than 6 months prior to positive chronic hep-b test (#5 or #6 or #7)
                    #
                    if (case_ah_obj and (case_ah_obj.status == 'HEP_B-A') and 
                        (hep_b_case.date < (event.date - relativedelta(months=6)))):
                            new_criteria = 'Criteria F: positive results for hep-b surface antigen, or hep-b e antigen, or hep-b viral dna, and patient fulfilled criteria for acute Hep B more than 6 months prior to the positve results'
                            updated = self.update_case(hep_b_case,new_criteria, event)
                # change acute hep-b case to chronic hep-b
                if updated:
                    counter += 1
                    CaseActiveHistory.update_or_create(
                        case=hep_b_case,
                        status="HEP_B-C",
                        date=hep_b_case.date,
                        change_reason="HEP_B-AC",
                        event_rec=event,
                    )
                    continue

            #
            # Patient has chronic hep-b
            #
            created = False
            if event.name != 'dx:hepatitis_b:chronic': # check for only chronic hep-b dx without labs and never acute
                created, this_case = self._create_case_from_event_obj(
                    condition=self.conditions[0],
                    criteria='Criteria E: positive hep-b surface antigen, or hep-b e antigen, or hep-b viral dna, and does not fulfill criteria for acute hep-b',
                    recurrence_interval=None,  # Does not recur
                    event_obj=event,
                    relevant_event_qs=hep_b_pos_qs,
                )
            if created:
                counter += 1
                CaseActiveHistory.create(
                        case=this_case,
                        status="HEP_B-C",
                        date=this_case.date,
                        change_reason="HEP_B-CI",
                        event_rec=event,
                )
        log.debug('Created %s new Chronic Hep B, definitions e,f,g' % counter)
        return counter

    def update_case(self, case, criteria, events):
        case.criteria = criteria
        case.events.add(events)
        if case.status in ['S','RS']:
            case.status = 'RQ'
        case.save()
        return True

    def report_field(self, report_field, case):
        reportable_fields = {
            'NA-56': 'NA-1739',
            'disease': 'NA-1781',
            '10187-3': 'NA-1738',
            'disease_status': self._get_disease_status(case),
            'symptom_obx': False,
            'na_trmt_obx': False,
        }

        return reportable_fields.get(report_field, None)

    def _get_disease_status(self, case):
        status = {'HEP_B-A': 'NAR-87', 'HEP_B-C': 'NAR-361'}
        return status.get(case.caseactivehistory_set.last().status, None)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Packaging
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def event_heuristics():
    return Hepatitis_B().event_heuristics


def disease_definitions():
    return [Hepatitis_B()]
